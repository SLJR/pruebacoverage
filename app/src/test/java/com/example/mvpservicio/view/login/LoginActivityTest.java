package com.example.mvpservicio.view.login;

import com.example.mvpservicio.interactor.login.InteractorImpl;
import com.example.mvpservicio.presenter.login.PresenterActivity;

import org.junit.Test;
import org.mockito.internal.matchers.Null;

import static org.junit.Assert.*;

public class LoginActivityTest {

    /**
     * Validacion con los datos correctos
     */
    @Test
    public void AutenticacionCorrecta() {

        PresenterActivity presenter = new PresenterActivity() {
            @Override
            public void authentication(final String  user, final String  password) {

            }
            /**
             * Validacion recibe un parametro booleano
             * @param status
             */
            @Override
            public void validacion(final boolean status) {
                assertTrue(status);
            }
        };
        /**
         * Instancia para validar los datos
         */
        final InteractorImpl interactor = new InteractorImpl(presenter);
        interactor.authentication("admin","Admin");
    }




    /**
     * Validacion con los usuario incorrecto
     * USUARIO INCORRECTO
     */
    @Test
    public void AutenticacionUsuarioIncorrecta() {

        PresenterActivity presenter = new PresenterActivity() {
            @Override
            public void authentication(final String  user, final String  password) {

            }
            /**
             * Validacion recibe un parametro booleano
             * @param status
             */
            @Override
            public void validacion(final boolean status) {
                assertFalse(status);
            }
        };
        /**
         * Instancia para validar los datos
         */
        final InteractorImpl interactor = new InteractorImpl(presenter);
        interactor.authentication("XXXXXXX","Admin");
    }





    /**
     * Validacion con los datos correctos
     * PASSWORD INCORRECTO
     */
    @Test
    public void AutenticacionPasswordIncorrecto() {

        PresenterActivity presenter = new PresenterActivity() {
            @Override
            public void authentication(final String  user, final String  password) {

            }
            /**
             * Validacion recibe un parametro booleano
             * @param status
             */
            @Override
            public void validacion(boolean status) {
                assertFalse(status);
            }
        };
        /**
         * Instancia para validar los datos
         */
        final InteractorImpl interactor = new InteractorImpl(presenter);
        interactor.authentication("admin","XXXXXX");
    }

    /**
     * Validacion con los datos correctos
     * SIN USUARIO
     */
    @Test
    public void AutenticacionSinUsuario() {

        PresenterActivity presenter = new PresenterActivity() {
            @Override
            public void authentication(final String  user, final String  password) {

            }
            /**
             * Validacion recibe un parametro booleano
             * @param status
             */
            @Override
            public void validacion(boolean status) {
                assertFalse(status);
            }
        };
        /**
         * Instancia para validar los datos
         */
        final InteractorImpl interactor = new InteractorImpl(presenter);
        interactor.authentication("","Admin");
    }



    /**
     * Validacion con los datos correctos
     * SIN DATOS
     */
    @Test
    public void AutenticacionSinDatos() {

        PresenterActivity presenter = new PresenterActivity() {
            @Override
            public void authentication(final String  user, final String  password) {

            }
            /**
             * Validacion recibe un parametro booleano
             * @param status
             */
            @Override
            public void validacion(boolean status) {
                assertFalse(status);
            }
        };
        /**
         * Instancia para validar los datos
         */
        final InteractorImpl interactor = new InteractorImpl(presenter);
        interactor.authentication("","");
    }









    /**
     * Validacion con los datos correctos
     * SIN PASSWORD
     */
    @Test
    public void AutenticacionSinPassword() {

        PresenterActivity presenter = new PresenterActivity() {
            @Override
            public void authentication(final String  user, final String  password) {

            }
            /**
             * Validacion recibe un parametro booleano
             * @param status
             */
            @Override
            public void validacion(boolean status) {
                assertFalse(status);
            }
        };
        /**
         * Instancia para validar los datos
         */
        final InteractorImpl interactor = new InteractorImpl(presenter);
        interactor.authentication("admin","");
    }


    /**
     * Validacion con los datos correctos
     * DATOS NULL
     */
    @Test
    public void AutenticacionDatosNull() {

         PresenterActivity presenter = new PresenterActivity() {
            @Override
            public void authentication(final String  user, final String  password) {

            }
            /**
             * Validacion recibe un parametro booleano
             * @param status
             */
            @Override
            public void validacion(boolean status) {
                assertFalse(false);
            }
        };
        /**
         * Instancia para validar los datos
         */
        final InteractorImpl interactor = new InteractorImpl(presenter);
        interactor.authentication(null , null);
    }




    /**
     * Validacion con los datos correctos
     * USUARIO NULL
     */
    @Test
    public void AutenticacionUsuarioNull() {

        PresenterActivity presenter = new PresenterActivity() {
            @Override
            public void authentication(final String  user, final String  password) {

            }
            /**
             * Validacion recibe un parametro booleano
             * @param status
             */
            @Override
            public void validacion(boolean status) {
                assertFalse(false);
            }
        };
        /**
         * Instancia para validar los datos
         */
        final InteractorImpl interactor = new InteractorImpl(presenter);
        interactor.authentication(null,"Admin");
    }




    /**
     * Validacion con los datos correctos
     * PASSWORD NULL
     */
    @Test
    public void AutenticacionPasswordNull() {

        PresenterActivity presenter = new PresenterActivity() {
            @Override
            public void authentication(final String  user, final String  password) {

            }
            /**
             * Validacion recibe un parametro booleano
             * @param status
             */
            @Override
            public void validacion(boolean status) {
                assertFalse(false);
            }
        };
        /**
         * Instancia para validar los datos
         */
        final InteractorImpl interactor = new InteractorImpl(presenter);
        interactor.authentication("admin",null);
    }




}