package com.example.mvpservicio;


import io.realm.RealmObject;

/**
 * Clase Usuario
 */
public class Usuario extends RealmObject {

    private String user;
    private String password;

    /**
     * Constructor solo con parametro
     * user
     * @param user
     */
    public Usuario(String user) {
        this.user = user;
    }

    /**
     * Constructor vacio
     */
    public Usuario() {

    }

    /**
     * Getter y setters de los
     * atributos user
     * y password
     * @return
     */
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
