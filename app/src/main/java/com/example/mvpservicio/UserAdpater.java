package com.example.mvpservicio;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mvpservicio.view.usuarios.UserAdapterActivity;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmResults;

public class UserAdpater extends RecyclerView.Adapter<UserViewHolderActivity> {


    /**
     * Lista de tipo Request para ingresar los datos
     * obtenidos
     */
     final List<Usuario> lista;
     List<Usuario> listaCopy1;

    Realm myRealm;
    RealmResults<Usuario> result;

    /**
     * Creacion de instancia de la interfaz
     * UserAdapterActivity, necesaria para la
     * implementacion del listener del Button
     */
    UserAdapterActivity usereAdapter;

    /**
     * COnstructor de la clase {UserAdapter}
     * que inicializa la lista de tipo Reques.
     *
     * Se agrego un listener para asignarle accion al boton
     * @param list
     */
    public UserAdpater(List<Usuario> list, UserAdapterActivity usereAdapter) {
        listaCopy1 =list;
        this.lista=listaCopy1;
        this.usereAdapter=usereAdapter;
    }

    /**
     *
     * Metodo para se Inflar la vista, y dicirle que layout va tener el adaptador
     */

    @NonNull
    @Override
    public UserViewHolderActivity onCreateViewHolder(@NonNull ViewGroup viewGroup, int typeView) {

        View view = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.activity_user_view_holder, viewGroup, false);
        return new UserViewHolderActivity(view);
    }

    /**
     * Metodo que agrega los datos al RecyclerView
     * para reutilizar espacio.
     * Cuenta con dos parametros, el objeto holder y la posicion.
     * @param holder
     * @param posicion
     */

    @Override
    public void onBindViewHolder(@NonNull UserViewHolderActivity holder, int posicion) {
        holder.bindName(lista.get(posicion).getUser());

         final String usuari=String.valueOf(lista.get(posicion).getUser());
        /**
         * Se agrega accion al boton del CardView para la eliminacion de los usuarios
         *
         */
        holder.tbEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * llamado del metodo
                 * para eliminar
                 */
                usereAdapter.eliminarUsuario(usuari);
            }
        });
    }

    /**
     * Metodo que retorna el tamaño de la lista.
     * @return
     */
    @Override
    public int getItemCount() {
        return lista.size();
    }


}
