package com.example.mvpservicio.presenter.fragment;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

/**
 * Interfaz que sera implementada
 * por la clase Presenter Impl
 */
public interface PresenterFragmentActivity {


    /** Metodo que consulta de la informacion*/
    void consultaInfo();
    void iniciaMensaje();
    void cierraMenaje();
    /** Metodo de exito  */
    void onResponse(JSONArray response);
    void onErrorResponse(VolleyError error);
    void adapterUpdate();
    /** Inicia la libreria Volley */
    void inicioLibreria();
    void enviaACola(JsonArrayRequest request);
}
