package com.example.mvpservicio.presenter.menu;

import com.example.mvpservicio.interactor.menu.InteractorIntMenuActivity;
import com.example.mvpservicio.interactor.menu.InteractorMenuImpl;
import com.example.mvpservicio.view.menu.ViewMenuActivity;

/**
 * Clase que implementa de la interfaz {PresenterMenuActivity}
 *
 */
public class PresenterMenuImpl implements PresenterMenuActivity {

    /**
     * Declaracion de instancias necesarias
     * para seguir el proceso
     */
    InteractorIntMenuActivity interactorMenu;
    ViewMenuActivity viewMenu;

    /**
     * Constructor para inicializar las
     * instancias
     * @param viewMenu
     */
    public PresenterMenuImpl(ViewMenuActivity viewMenu) {
        this.viewMenu = viewMenu;

        interactorMenu = new InteractorMenuImpl(this);

    }

    /**
     * Obtencion del metdodo abreFregment
     * de la vista
     */

    @Override
    public void lanzaFragment() {
        viewMenu.abreFragment();
    }
}