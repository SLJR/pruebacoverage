package com.example.mvpservicio.presenter.login;

/**
 * Interfas {@link PresenterActivity} con
 * metodos implementados en la clase {@link PresenterImpl}
 */
public interface PresenterActivity {

    /**
     * Metodo de autenticación
     * recibe los siguientes parametros:
     * @param user
     * @param password
     */
    void authentication(String user, String password);

    /**
     * Metodo a de validacion
     * recibe como parametro lo siguiente:
     * @param status
     */
    void validacion(boolean status);


}
