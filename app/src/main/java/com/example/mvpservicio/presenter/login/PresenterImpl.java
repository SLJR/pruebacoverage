package com.example.mvpservicio.presenter.login;


import com.example.mvpservicio.interactor.login.InteractorActivity;
import com.example.mvpservicio.interactor.login.InteractorImpl;
import com.example.mvpservicio.view.login.ViewActivity;

/**
 * Clase {@link PresenterImpl} implementa de la
 * interfaz {@link PresenterActivity}, los metodos para
 * la comunicacion con la vista y con el interactor
 */
public class PresenterImpl implements PresenterActivity {

    /**
     * Instancia de la interfaz {@link InteractorActivity}
     * */
    InteractorActivity interactor;
    /**
     * Instancia de la interfaz {@link ViewActivity}
     * */
    ViewActivity viewACtivity;


    /**
     * Constructor de la clase {@link PresenterImpl}
     * En el cual se inicializan la instanca:
     * @param viewACtivity
     *
     * De iigual forma se inicializa la instancia
     * de la interfaz {@link InteractorActivity}
     */
    public PresenterImpl(ViewActivity viewACtivity) {
        this.viewACtivity = viewACtivity;
        interactor = new InteractorImpl(this);

    }

    /**
     * Metodo sobreescrito de la interfaz
     *
     * {@link PresenterActivity}
     * @param user
     * @param password
     */
    @Override
    public void authentication(String user, String password){
        interactor.authentication(user,password);

    }


    /**
     * Metodo sobreescrito de la interfaz
     * {@link PresenterActivity} que ejecuta un metodo
     * de la clase {@link com.example.mvpservice.view.LoginActivity}
     * @param status
     */

    @Override
    public void validacion(boolean status) {
        viewACtivity.validacion(status);

    }



}
