package com.example.mvpservicio.presenter.usuarios;

import com.example.mvpservicio.interactor.usuarios.InteractorUserActivity;
import com.example.mvpservicio.interactor.usuarios.InteractorUserImpl;
import com.example.mvpservicio.view.registro.ViewRegActivity;
import com.example.mvpservicio.view.usuarios.UserAdapterActivity;

/**
 * Clase PresenterUserImpl
 */
public class PresenterUserImpl implements PresenterUserActivity{

    /**
     * Creacion de las instancias
     * necesarias para la clase
     */
    UserAdapterActivity viewUsuario;
    ViewRegActivity viewRegActivity;
    InteractorUserActivity interactor;

    /**
     * Constructor de la clase
     * e inicializacion de las instancias
     * @param viewUsuario
     */
    public PresenterUserImpl(UserAdapterActivity viewUsuario) {
        this.viewUsuario = viewUsuario;
        interactor = new InteractorUserImpl(this);
    }


    /**
     * Metodo de eliminacion de usuarios
     * implementado en el interactor
     * @param usuario
     */
    @Override
    public void eliminarUsuario(String usuario) {
        interactor.eliminarUsuario(usuario);
    }


}
