package com.example.mvpservicio.presenter.usuarios;

/**
 * Interfaz creada para la clase PresenterUserImpl
 */
public interface PresenterUserActivity {
    /**
     * Metodo a implementar
     * para la eliminacion de usuarios
     * @param usuario
     */
    void eliminarUsuario(String usuario);


}
