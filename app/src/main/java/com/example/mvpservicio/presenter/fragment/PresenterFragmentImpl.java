package com.example.mvpservicio.presenter.fragment;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.mvpservicio.interactor.fragment.InteractorFragmentActivity;
import com.example.mvpservicio.interactor.fragment.InteractorFragmentImpl;
import com.example.mvpservicio.view.fragment.ViewFragmentActivity;

import org.json.JSONArray;


/**
 *Clase puente entre la vista y el Interactor
 */

public class PresenterFragmentImpl implements PresenterFragmentActivity{
    final InteractorFragmentActivity interactorFrag;
    final ViewFragmentActivity viewFragment;

    public PresenterFragmentImpl(ViewFragmentActivity viewFragment) {
        this.viewFragment = viewFragment;
        interactorFrag = new InteractorFragmentImpl(this);
    }

    /**
     *  Metiodo que ejecuta la consulta de información
     *  mediante la libreria Volley
     *  */
    @Override
    public void consultaInfo() {
        interactorFrag.consultaInfo();

    }

    /**
     * Obtiene el metodo iniciaMensaje
     * del fragmenet {ServiceFragment}
     */
    @Override
    public void iniciaMensaje() {
        viewFragment.iniciaMensaje();

    }

    /**
     * Obtiene el metodo cierraMensaje
     * del fragmenet {ServiceFragment}
     */
    @Override
    public void cierraMenaje() {
        viewFragment.cierraMenaje();
    }
    /**
     * Obtiene el metodo onResponse
     * del fragmenet {ServiceFragment}
     */
    @Override
    public void onResponse(JSONArray response) {
        viewFragment.onResponse(response);
    }
    /**
     * Obtiene el metodo onErrorResponse
     * del fragmenet {ServiceFragment}
     */
    @Override
    public void onErrorResponse(VolleyError error) {
        viewFragment.onErrorResponse(error);
    }

    /**
     * Obtiene el metodo adapterUpdate
     * del fragmenet {ServiceFragment}
     */
    @Override
    public void adapterUpdate() {
        viewFragment.adapterUpdate();
    }
    /**
     * Obtiene el metodo inicioLibreria
     * del fragmenet {ServiceFragment}
     */
    @Override
    public void inicioLibreria() {
        viewFragment.inicioLibreria();

    }
    /**
     * Obtiene el metodo enviaACola
     * del fragmenet {ServiceFragment}
     */
    @Override
    public void enviaACola( JsonArrayRequest request) {
        viewFragment.enviaACola(request);

    }

}
