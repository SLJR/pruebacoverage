package com.example.mvpservicio.presenter.registro;

/**
 * Interfaz creada para la clase
 * PresenterRegImpl
 */
public interface PresenterRegActivity {

    /**
     *
     * Metodos necesarios
     * para el funcionamiento de la clase PresenterRegIMpl
     * @param user
     * @param password
     */
    void transaccionReg(String user, String password);

    /**
     * Metodos para el envio de
     * mensajes de error
     */
    void mesnajeUsuarioExiste();
    void usuarioInvalido();
    void usuarioCreado();

}
