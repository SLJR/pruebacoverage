package com.example.mvpservicio.presenter.registro;

import com.example.mvpservicio.interactor.resgistro.InteractarRegImpl;
import com.example.mvpservicio.interactor.resgistro.InteractorRegActivity;
import com.example.mvpservicio.view.registro.ViewRegActivity;

/**
 * Clase Presenter
 */
public class PresenterRegImpl implements PresenterRegActivity{

    /**
     * Creacion de las instancias
     * de las interfaces de la vista
     * y del interactor
     */
     final InteractorRegActivity interactor;
     final ViewRegActivity viewRegActivty;

    /**
     * constructor de la clase.
     * Se incializan las instancias.
     * @param viewRegActivty
     */
    public PresenterRegImpl(ViewRegActivity viewRegActivty) {
        this.viewRegActivty = viewRegActivty;
        interactor = new InteractarRegImpl(this);
    }

    /**
     * Metodo de registro
     * @param user
     * @param password
     */
    @Override
    public void transaccionReg(String user, String password) {
        interactor.transaccionReg(user,password);

    }

    /**
     * Mensaje si usuario ya esta registrado
     */
    @Override
    public void mesnajeUsuarioExiste() {
        viewRegActivty.mesnajeUsuarioExiste();

    }


    /**
     * Mensaje si el usuario es
     * invalido (null o vacio)
     */
    @Override
    public void usuarioInvalido() {
        viewRegActivty.usuarioInvalido();
    }

    /**
     * Mensaje si el usuario
     * fue creado correctamente.
     */
    @Override
    public void usuarioCreado() {
        viewRegActivty.usuarioCreado();

    }

}
