package com.example.mvpservicio.presenter.menu;

/**
 *
 * Interfaz que implementara la clase Presenter Impl
 */
public interface PresenterMenuActivity {

    /**
     * Metodo lanzarFragment
     * obtenido de la Vista
     */
    void lanzaFragment();

}
