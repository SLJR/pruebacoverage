package com.example.mvpservicio;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import android.widget.Button;
import android.widget.TextView;
/**
 * Clase {UserViewHolderActivity} extiende de la clase
 * {RecyclerView.ViewHolder}
 */

public class UserViewHolderActivity extends RecyclerView.ViewHolder {

    /**
     * Declaración de TextView
     */

    public TextView tvUsuario;
    public Button tbEliminar;

    /**
     * Constructor donde se agrega el View
     * y se inicializa la variable tvAdapterName
     * @param itemView
     */

    public UserViewHolderActivity(@NonNull View itemView) {
        super(itemView);
        tvUsuario = itemView.findViewById(R.id.tvUsuarioCV);
        tbEliminar = itemView.findViewById(R.id.tbEliminar);
    }

    /**
     * Asignacion de la etiqueta concatenada
     * a la variable que tendra el componente.
     * Recibe el parametro:
     * @param
     */
    public void bindName(String usuario) {

        tvUsuario.setText(usuario);

    }



}
