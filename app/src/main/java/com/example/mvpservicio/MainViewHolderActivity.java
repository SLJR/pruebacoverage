package com.example.mvpservicio;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Clase {MainViewHolderActivity} extiende de la clase
 * {RecyclerView.ViewHolder}
 */

public class MainViewHolderActivity extends RecyclerView.ViewHolder {

    /**
     * Declaración de TextView
     */
    public TextView tvAdapterName;

    /**
     * Constante ETIQUETA es ocupada para
     * contextualizar los datos obtenidos por
     * el consumo del servicio.
     */
    public final static String ETIQUETA = "Nombre: ";

    /**
     *
     * Constructor donde se agrega el View
     * y se inicializa la variable tvAdapterName
     * @param itemView
     */
    public MainViewHolderActivity(@NonNull View itemView) {
        super(itemView);

        tvAdapterName = itemView.findViewById(R.id.tvAdapterName);

    }

    /**
     * Asignacion de la etiqueta concatenada
     * a la variable que tendra el componente.
     * Recibe el parametro:
     * @param name
     */
    public void bindName(String name) {

        tvAdapterName.setText(ETIQUETA + name);

    }

}

