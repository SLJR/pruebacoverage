package com.example.mvpservicio.interactor.fragment;

/**
 * interfaz que implementa la clase de
 * interactor
 */
public interface InteractorFragmentActivity {

    /**
     * Mteodo que obtiene la información
     * del servicio.
     */
    void consultaInfo();

}

