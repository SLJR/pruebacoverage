package com.example.mvpservicio.interactor.usuarios;

import com.example.mvpservicio.Usuario;
import com.example.mvpservicio.presenter.usuarios.PresenterUserActivity;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Clase Interactoruser
 */
public class InteractorUserImpl implements InteractorUserActivity {

    /**
     *
     * Creacion de instancias necesarias
     * para la ejecucion de la clase
     */
    PresenterUserActivity presenter;
    Realm myRealm;
    public InteractorUserImpl(PresenterUserActivity presenter) {
        this.presenter = presenter;
            myRealm = Realm.getDefaultInstance();
    }

    /**
     * Metodo implementado
     * para la eliminacion del usuario
     * de la base de datos Realm
     * COmienzo y terminacion de la trasaccion realm
     * @param user
     */
    @Override
    public void eliminarUsuario(String user) {
        myRealm.beginTransaction();
        RealmResults<Usuario> result = myRealm.where(Usuario.class).equalTo("user", user).findAll();
        result.deleteAllFromRealm();
        myRealm.commitTransaction();
    }
}
