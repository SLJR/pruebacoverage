package com.example.mvpservicio.interactor.usuarios;

/**
 * Interfaz creada para
 * la clase InteractorUserImpl
 */

public interface InteractorUserActivity {

    /**
     * Metodo creado para ser
     * implementado por la clase Impl
     * @param user
     */
    void eliminarUsuario(String user);

}
