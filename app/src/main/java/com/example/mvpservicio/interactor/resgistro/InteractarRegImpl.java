package com.example.mvpservicio.interactor.resgistro;

import com.example.mvpservicio.Usuario;
import com.example.mvpservicio.presenter.registro.PresenterRegActivity;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 *
 * Clase interactor para
 * la parte del registro de usuario
 *
 */

public class InteractarRegImpl implements InteractorRegActivity {

    /**
     * Creacion de instancias
     * que ocupara la clase
     */
    PresenterRegActivity presenter;
    /**
     * Instancia Realm
     */
    Realm myRealm;

    public InteractarRegImpl(PresenterRegActivity presenter) {
        this.presenter = presenter;
        myRealm= Realm.getDefaultInstance();

    }

    /**
     * Metodo que realiza la alta de usuarios
     * en la aplicacion, recibe dos parametros:
     * @param user
     * @param password
     */
    @Override
    public void transaccionReg(String user, String password) {
        /**
         * Comienzo y cierre de la
         * transaccion Realm
         */
        myRealm.beginTransaction();
        RealmResults<Usuario> result= myRealm.where(Usuario.class).findAll();
        myRealm.commitTransaction();
        boolean existe=false;
        String vacio ="";
        /**
         * fragmento de codigo que valida
         * que los datos no vengan en null
         * o vacios
         */
        if((user== null || password==null)||(user.equals(vacio)||(password.equals(vacio)))){
            presenter.usuarioInvalido();
        }else{
            /**
             * Validacion de la existencia
             * de usuarios ya registrados
             */
            for (Usuario usuario:result) {
                if(usuario.getUser().equals(user)){
                    existe=true;
                }
            }
            if(existe){
                presenter.mesnajeUsuarioExiste();
            }else{
                /**
                 * Creacion de la transaccion que
                 * genera la alta de los usuarios a
                 * Realm
                 */
                myRealm.beginTransaction();
                Usuario myUser= myRealm.createObject(Usuario.class);
                myUser.setUser(user);
                myUser.setPassword(password);
                myRealm.commitTransaction();
                presenter.usuarioCreado();
            }
        }
    }
}

