package com.example.mvpservicio.interactor.menu;

import com.example.mvpservicio.presenter.menu.PresenterMenuActivity;

public class InteractorMenuImpl implements InteractorIntMenuActivity{
    /**
     * Instancia de la interfaz
     * {@link PresenterMenuActivity}
     */

    private final PresenterMenuActivity presenterMenu;

    /**
     *
     * Constructor de la calse {InteractorMenuImpl}
     * inicicializando a la instancia de la clase {@link PresenterMenuActivity}
     * @param presenterMenu
     */
    public InteractorMenuImpl(PresenterMenuActivity presenterMenu) {
        this.presenterMenu = presenterMenu;
    }

    /**
     * Ejecuta lafuncion lanzaFragment
     * del presentador
     */

    @Override
    public void lanzaFragment() {
        presenterMenu.lanzaFragment();

    }
}

