package com.example.mvpservicio.interactor.login;
/**
 * interfaz Interactor
 */
public interface InteractorActivity {

    /**
     * Metodo para autenticar las credenciales ingresadas.
     * @param user
     * @param password
     */
    void authentication(String user, String password);

}
