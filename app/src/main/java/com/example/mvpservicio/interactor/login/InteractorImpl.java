package com.example.mvpservicio.interactor.login;


import com.example.mvpservicio.Usuario;
import com.example.mvpservicio.presenter.login.PresenterActivity;

import io.realm.Realm;
import io.realm.RealmResults;

public class InteractorImpl implements InteractorActivity {


    /**
     * Varibales para comparacion
     * de credenciales obtenidas por interfaz
     */
    public static final String USUARIO ="admin";
    public static final String  PASSWORD ="Admin";
    Realm myRealm;
    RealmResults<Usuario> result;



    /**
     * Instancia de la interfaz {@link PresenterActivity}
     * */
    PresenterActivity presenterIntyerface;

    /**
     * Constructor de la clase que inicializa
     * la instacia de la interfaz {@link PresenterActivity}
     * @param presenter
     */
    public InteractorImpl(PresenterActivity presenter) {
        this.presenterIntyerface = presenter;
        myRealm= Realm.getDefaultInstance();

    }

    /**
     * Metodo para la utenticacion con
     * usuarios de la base de datos REALM
     * el metodo cuenta con dos paremtros, que son
     * usuarui y password
     * @param user
     * @param password
     */


    @Override
    public void authentication(String user, String password) {

        result= myRealm.where(Usuario.class).findAll();
        boolean correcto=false;
        for (Usuario usuario : result){
            if (user.equals(usuario.getUser()) && password.equals(usuario.getPassword())){
                correcto =true;
            }
        }

        /**
         * Vaidacion de la bandera
         * indicadora
         */
        if (correcto) {
            presenterIntyerface.validacion(true);
        }else {
            presenterIntyerface.validacion(false);

        }

    }

}
