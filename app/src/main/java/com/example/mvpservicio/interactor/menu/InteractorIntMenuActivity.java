package com.example.mvpservicio.interactor.menu;

/**
 * interfaz creada para ser implmenentada
 * por la clase Interactor Impl
 */

public interface InteractorIntMenuActivity {

        /**
         * Metodo que obtiene
         * los datos del servicio.
         */
        void lanzaFragment();


}
