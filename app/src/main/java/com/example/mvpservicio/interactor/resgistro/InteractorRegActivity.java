package com.example.mvpservicio.interactor.resgistro;

/**
 * Interfaz creada
 * para la clase InteractorRegActivity
 */
public interface InteractorRegActivity {

    /**
     * Metodo para agregar usuarios a REALM
     * @param user
     * @param password
     */
    void transaccionReg(String user, String password);


}
