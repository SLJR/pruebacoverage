package com.example.mvpservicio.interactor.fragment;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.mvpservicio.presenter.fragment.PresenterFragmentActivity;

import org.json.JSONArray;

/**
 *
 * Clase que implementa los metodos de la interface.
 * Realiza la autenticacion del login.
 *
 */

public class InteractorFragmentImpl implements InteractorFragmentActivity {


    private final static String BASE_URL = "https://api.github.com/users/fsaldivar/repos";

    /**
     * Declaracion de instancia de la interfaz
     * {@link PresenterFragmentActivity} y
     * {@link JsonArrayRequest}
     */
    PresenterFragmentActivity presentFragment;

    public InteractorFragmentImpl(PresenterFragmentActivity presentFragment) {
        this.presentFragment = presentFragment;
    }


    @Override
    public void consultaInfo() {
        presentFragment.iniciaMensaje();
        /**
         * inicializa la libreria de Volley
         */
        presentFragment.inicioLibreria();
        /**
         * Arreglo de json(BASE_URL,lisener respues correcta, cerrar resouesa incorrecta)
         */
        JsonArrayRequest request = new JsonArrayRequest(BASE_URL,  new Response.Listener<JSONArray>() {
            /**
             * Metodo listener con la opcion correcta
             *
             */
            @Override
            public void onResponse(JSONArray response) {
                presentFragment.cierraMenaje();
                presentFragment.onResponse(response);

                /**
                 * Actualiza el adaptador
                 */
                presentFragment.adapterUpdate();
            }

        }, new Response.ErrorListener() {
            /**
             * Metodo lostener con la opcion erronea.
             */
            @Override
            public void onErrorResponse(VolleyError error) {
                presentFragment.onErrorResponse(error);
            }
        });
        //envie a la cola esta peticion.
        presentFragment.enviaACola(request);
    }
}
