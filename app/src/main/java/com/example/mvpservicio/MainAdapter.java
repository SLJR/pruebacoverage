package com.example.mvpservicio;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainViewHolderActivity> {

    /**
     * Lista de tipo Request para ingresar los datos
     * obtenidos
     */
     final List<Request> lista;
       List<Request> listaCopy1;

    /**
     * COnstructor de la clase { MainAdapter}
     * que inicializa la lista de tipo Reques.
     *
     */

    public MainAdapter(final List<Request> list) {
        listaCopy1 =list;
        this.lista=listaCopy1;

    }

    /**
     *
     * Metodo para se Inflar la vista, y dicirle que layout va tener el adaptador
     */

    @NonNull
    @Override
    public MainViewHolderActivity onCreateViewHolder(@NonNull ViewGroup viewGroup, int typeView) {

        View view = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.activity_main_view_holder, viewGroup, false);
        return new MainViewHolderActivity(view);
    }


    /**
     * Metodo que agrega los datos al RecyclerView
     * para reutilizar espacio.
     * Cuenta con dos parametros, el objeto holder y la posicion.
     * @param holder
     * @param posicion
     */

    @Override
    public void onBindViewHolder(@NonNull MainViewHolderActivity holder, int posicion) {
        holder.bindName(lista.get(posicion).getName());

    }

    /**
     * Metodo que retorna el tamaño de la lista.
     * @return
     */
    @Override
    public int getItemCount() {
        return lista.size();
    }
}
