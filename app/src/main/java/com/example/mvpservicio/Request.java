package com.example.mvpservicio;

import com.google.gson.annotations.SerializedName;

    /**
     * Clase Request, declaracion de campos
     * para realizar el consumo del servicio.
     *
     */
    public class Request{

        @SerializedName("name")
        private String name;

        /**
         * Getter para obtencio del dato
         * @return name
         */
        public String getName() {

            return name;
        }

        /**
         * Setter para asignacion del dato
         * @return name
         */
        public void setName(String name) {

            this.name = name;
        }

    }
