package com.example.mvpservicio;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Clase para Realm
 * necesita que extienda de Application
 */

public class RealmApp extends Application {

    /**
     * EL metodo sobreescrito
     * onCreate se crea la configuracion
     * del servicio Realm
     * para poder manejar la base de datos.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration config= new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(config);
    }
}
