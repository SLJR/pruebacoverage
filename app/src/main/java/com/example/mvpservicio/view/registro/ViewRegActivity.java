package com.example.mvpservicio.view.registro;

/**
 *Interfaz creada para la clase RegistroActivity
 */

public interface ViewRegActivity {

    /**
     * Metodos empleados para el envio de mensajes
     * durante la ejecucion de la
     * aplicación.
     */
    void mesnajeUsuarioExiste();
    void usuarioInvalido();
    void usuarioCreado();




}
