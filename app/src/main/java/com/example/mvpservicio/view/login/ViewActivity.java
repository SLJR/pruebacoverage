package com.example.mvpservicio.view.login;
/**
 *
 * Interfaz creada para la clase de
 * {@link LoginActivity}
 */
public interface ViewActivity {

    /**
     * Metodos  que deberan ser
     * implementados por la clase {@link LoginActivity}
     */
    void openActivity();
    void validacion(boolean status);
    void mensajeExito(String mensaje);
    void mensajeError(String mensaje);


}
