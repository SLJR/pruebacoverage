package com.example.mvpservicio.view.fragment;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

/**
 *
 * Interfaz a implementar
 * por la clase ServiceFragment
 */

public interface ViewFragmentActivity {

    /**
     * Lista demetodos
     * que la clase que implemente de esta
     * interfaz debera implementara
     */
    void consultaInfo();
    void iniciaMensaje();
    void cierraMenaje();
    void onResponse(JSONArray response);
    void onErrorResponse(VolleyError error);
    void adapterUpdate();
    void inicioLibreria();
    void enviaACola(JsonArrayRequest request);
}
