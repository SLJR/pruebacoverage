package com.example.mvpservicio.view.registro;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mvpservicio.R;
import com.example.mvpservicio.presenter.registro.PresenterRegActivity;
import com.example.mvpservicio.presenter.registro.PresenterRegImpl;

/**
 * Activity Registro, implementa de la interfas
 * VieRegActivity
 */

public class RegistroActivity extends AppCompatActivity implements ViewRegActivity {

    /**
     * Declaracion de componentes
     * empleado en la clase
     */

    private  EditText etUser;
    private  EditText etPassword;
    private  Button tbSave;
    /**
     * Creacion de instancia de la interfaz del presenter
     */
    private PresenterRegActivity presenter;

    /**
     * Creacion de metodo onCreate
     * en el cual se inicializan los componentes y se infla la vista
     *
     * @param savedInstanceState
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        etUser = findViewById(R.id.etUserReg);
        etPassword=findViewById(R.id.etPasswordReg);
        tbSave= findViewById(R.id.btSave);
        /**
         * Inicializacion de la instancia del presenter
         */
        presenter = new PresenterRegImpl(this);
        /** Accion del Button guardar  */
        tbSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Ejecucion del metodo
                 * para ingresar los usuarios
                 */
                String usuario = etUser.getText().toString();
                String password = etPassword.getText().toString();
                presenter.transaccionReg(usuario,password);

            }
        });
    }
    /**
     * Implementacion del metodo
     * cuandooel usuario ya existe
     */
    @Override
    public void mesnajeUsuarioExiste() {
        AlertDialog.Builder builder = new AlertDialog.Builder(RegistroActivity.this);
        builder.setTitle("Error");
        builder.setMessage("Usuario ya existe....");
        builder.setPositiveButton("Reintentar",null);
        builder.show();

    }

    /**
     * Implementa el metodo
     * cuando se recibe un usuario
     * incorrecto, null o vacio algun campo requerrido
     */
    @Override
    public void usuarioInvalido(){
        AlertDialog.Builder builder = new AlertDialog.Builder(RegistroActivity.this);
        builder.setTitle("Error");
        builder.setMessage("Usuario  Invalido....");
        builder.setPositiveButton("Reintentar",null);
        builder.show();
    }

    /**
     * Implementacion de metodo
     * cuando se creo correctamente
     * el usuario
     */
    @Override
    public void usuarioCreado() {
        AlertDialog.Builder builder = new AlertDialog.Builder(RegistroActivity.this);
        builder.setTitle("Correcto");
        builder.setMessage("Usuario se ha creado....");
        builder.setPositiveButton("Aceptar",null);
        builder.show();
       // finish();

    }
}
