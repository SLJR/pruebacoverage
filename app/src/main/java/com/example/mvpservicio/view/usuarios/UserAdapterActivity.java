package com.example.mvpservicio.view.usuarios;


/**
 * Interfaz creada para acceder a metodos
 * de la vista en el adaptador.
 * Y sera implementada por la vista de igual manera
 */
public interface UserAdapterActivity {

    /**
     * Metodo a implementar
     * @param usuario
     */
    void eliminarUsuario(String usuario);


}
