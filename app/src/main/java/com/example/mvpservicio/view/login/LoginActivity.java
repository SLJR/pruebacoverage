package com.example.mvpservicio.view.login;


import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.mvpservicio.R;
import com.example.mvpservicio.view.registro.RegistroActivity;
import com.example.mvpservicio.presenter.login.PresenterActivity;
import com.example.mvpservicio.presenter.login.PresenterImpl;
import com.example.mvpservicio.view.menu.MenuActivity;

/**
 *Clase {LoginActivity} controla la vista principal de la aplicación
 * implementa de la interfaz  {@link ViewActivity}
 */

public class LoginActivity extends AppCompatActivity implements ViewActivity{

    /**
     *  Declaracion de variables, componentes y
     *  declaracion de una instancia de la interfaz
     * {PresenterActivity}
     */

    Intent openIntent;
    PresenterActivity presenter;
    private EditText etUsuario;
    private EditText etPassword;
    private Button btIngresar;
    int intentos = 4;
    TextView tvRegistro;

    /**
     * Metodo sobreescrito onCreate donde
     * se declara el View para la vista.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /**
         * inicializacion de componentes utilizados
         * en el layout. Inicialización de la instancia
         * presenter de la interfaz  {PresenterActivity}
         */
        etUsuario = findViewById(R.id.etUsuario);
        etPassword = findViewById(R.id.etPassword);
        btIngresar = findViewById(R.id.btIngresar);
        tvRegistro= findViewById(R.id.tvRegistro);
        presenter = new PresenterImpl(this);

        /**
         * Implementación del Listener onClickListener
         * al boton btIngresar para iniciar la validación
         * de las credenciales
         *
         */

        btIngresar.setOnClickListener( new View.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                /**
                 * Obtencion de los tados contenidos en los TextView
                 * correspondientes al usuario y password.
                 */
                String user = etUsuario.getText().toString();
                String password = etPassword.getText().toString();
                /**
                 * Inicializacion de la instancia presenter*/
                presenter.authentication(user,password);
            }


        });

        /**
         *
         *
         */

        tvRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_reg = new Intent(LoginActivity.this,RegistroActivity.class);
                LoginActivity.this.startActivity(intent_reg);
            }
        });


    }

    /**
     * Metodo que muestra menu, ejecutado una vez validados los datos
     * ingresados
     */
    @Override
    public void openActivity() {

        openIntent = new Intent(LoginActivity.this, MenuActivity.class);
        LoginActivity.this.startActivity(openIntent);
    }


    /**
     * verificacion de la bandera que indica la
     * igualdad de los datos ingresados.
     * @param status
     */

    @Override
    public void validacion(boolean status) {
        if(status){
            mensajeExito("Credenciales Correctas!!!!");
            openActivity();

        }else{

            mensajeError("Credenciales Incorrectas");

        }
    }

    /**
     * Metodo que muestra un mensaje de exito
     * al ser exitosa la validacion, recibe un parametro
     * @param mensaje que es el mensaje a mostrar.
     */
    @Override
    public void mensajeExito(final String mensaje) {
        String saludo = "¡Bienvenido!";
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle(saludo);
        builder.setMessage(mensaje);
        builder.show();

    }
    /**
     * Metodo que muestra un mensaje de error
     * al no ser  exitosa la validacion, al no ingresar
     * las credenciales correctas por 3 intentos, se cierra la aplicacion.
     * Recibe un parametro
     * @param mensaje que es el mensaje a mostrar.
     *
     */
    @Override
    public void mensajeError(final String mensaje) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("¡Alerta!");
        builder.setMessage(mensaje);
        builder.show();
        intentos--;

        if (intentos == 0) {
            btIngresar.setEnabled(false);
            AlertDialog.Builder builderIntento = new AlertDialog.Builder(LoginActivity.this);
            builderIntento.setTitle("¡Alerta!");
            builderIntento.setMessage("No tienes mas intentos!..");
            builderIntento.show();
            finish();

        }
    }
}
