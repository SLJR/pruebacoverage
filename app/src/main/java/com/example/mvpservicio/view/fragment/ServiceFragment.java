package com.example.mvpservicio.view.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.mvpservicio.MainAdapter;
import com.example.mvpservicio.R;
import com.example.mvpservicio.Request;
import com.example.mvpservicio.presenter.fragment.PresenterFragmentActivity;
import com.example.mvpservicio.presenter.fragment.PresenterFragmentImpl;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment en el que se mostrara la informacion obtenida del consumo del servicio
 */
public class ServiceFragment extends Fragment implements ViewFragmentActivity{

    /**
     * Delcaración de lista de tipo Request
     * Declaración una instancia de la clase {MainAdapter}
     * y otra de la clase ProgressDialog
     *
     */
    /**
     * Constante BASE_URL contiene la direccion del api
     * que se va a consumir
     */


    private  MainAdapter adapter;
    private  PresenterFragmentActivity presenterFrag;
    private final List<Request> names = new ArrayList<>();
    private  ProgressDialog progressDialog;
    private  RequestQueue queue;

    /**
     * Constructor vacio para
     */
    public ServiceFragment() {

        // Required empty public constructor
    }

    /**
     * Creacion del layout e inicilizacion del
     * adapter y del progressDialog.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista= inflater.inflate(R.layout.fragment_service, container, false);

        presenterFrag = new PresenterFragmentImpl(this);




        /**
         *Iniciacializando la instancia de la clase
         * { MainAdapter} pasandole la lista como
         * parametro
         */
        adapter = new MainAdapter(names);

        /**
         *Creacion e implementacion del componente
         * {RecyclerView} acoplandolo a la pantalla del
         * dispositivo, asignadole un llinearLayout y
         * agregando el adaptador al reciycler
         */
        RecyclerView list = vista.findViewById(R.id.rvlist);
        list.setHasFixedSize(true);
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        list.setAdapter(adapter);

        /**
         *
         * Ejecucion del metodo
         * que consume el servicio.
         */
        consultaInfo();

        return vista;
    }
/**
    @Override
    public void consultaInfo() {

        presenterFrag.consultaInfo();
    }*/

    @Override
    public void consultaInfo() {
    presenterFrag.consultaInfo();

    }

    /**
     * inicia mensaje de
     * progressDialog
     */
    @Override
    public void iniciaMensaje() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Consultando información");
        progressDialog.setMessage("Espere por favor...");
        progressDialog.show();

    }

    /**
     * Cierra mensaje de
     * progressDialog
     */
    @Override
    public void cierraMenaje() {
//        progressDialog = new ProgressDialog(getContext());
//        progressDialog.setTitle("Consultando información");
//        progressDialog.setMessage("Espere por favor...");
        progressDialog.dismiss();
    }

    /**
     * mMetodo  de opcion correcta
     * para el JsonArray Request
     * progressDialog
     */
    @Override
    public void onResponse(JSONArray response) {
        for (int index = 0; index < response.length(); index++) {
            try {
                //
                JSONObject json = response.getJSONObject(index);
                //parsear json a un request
                Request req = new Gson().fromJson(json.toString(), Request.class);
                //agregar cada request al array name
                names.add(req);
            } catch (JSONException e) {
                new RuntimeException(e);
                e.printStackTrace();
            }
        }
        presenterFrag.cierraMenaje();
        presenterFrag.adapterUpdate();
    }

    /**
     * mMetodo  de opcion erronea
     * para el JsonArray Request
     * progressDialog
     */
    @Override
    public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
        presenterFrag.cierraMenaje();
        Toast.makeText(getActivity(), "Ocurrio un error", Toast.LENGTH_SHORT).show();
    }

    /**
     * Metodo para actualizar el adaptador
     */
    @Override
    public void adapterUpdate() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void inicioLibreria() {
        queue = Volley.newRequestQueue(getContext());
    }

    @Override
    public void enviaACola(JsonArrayRequest request) {
        queue.add(request);

    }
}
