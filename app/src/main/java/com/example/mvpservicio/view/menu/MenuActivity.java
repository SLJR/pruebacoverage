package com.example.mvpservicio.view.menu;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.mvpservicio.R;
import com.example.mvpservicio.presenter.menu.PresenterMenuActivity;
import com.example.mvpservicio.presenter.menu.PresenterMenuImpl;
import com.example.mvpservicio.view.fragment.ServiceFragment;
import com.example.mvpservicio.view.login.LoginActivity;
import com.example.mvpservicio.view.usuarios.UsuarioFragment;

/**
 * Clase para el menu deslizable implementa de la clase
 * {@link NavigationView.OnNavigationItemSelectedListener}
 *
 */

public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ViewMenuActivity {


    /**
     * Instancias de los objetos que se
     * utilizan en el proceso.
     */
    private FragmentManager fragMana;
    private PresenterMenuActivity presenterMenu;

    /**
     * Metodo pra la creación de la actividad
     * e inicializacion de componentes.
     * @param savedInstanceState
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        presenterMenu = new PresenterMenuImpl(this);


        /**
         * Decaración del objeto Toolbar e inicializacion.
         */
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /**
         * Declaracion de {@link DrawerLayout} y el
         * {@link NavigationView} para el lateral del menu.
         *
         */
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        /**
         * creacion del ActionBar y
         * asignacion al {@link DrawerLayout}
         */
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    /**
     * Metodo que permite abrir y cerrar
     * el desplazamiento del menu lateral
     */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     *Metodo que identifica que tipo de elemento del menu
     * ha sido pulsado, y efectua la
     * accion correspondiente.
     * @RequiereApi
     */

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_consulta) {
            presenterMenu.lanzaFragment();
        } else if(id == R.id.nav_usuarios){

            fragMana = getSupportFragmentManager();
            fragMana.beginTransaction().replace(R.id.contenedor, new UsuarioFragment()).commit();
        } else if (id == R.id.nav_salir) {
            finish();
        }else if (id == R.id.nav_cierra) {
            Intent i = new Intent(MenuActivity.this, LoginActivity.class);
            startActivity(i);
        }

        /**
         * Inicializacion del DrawerLayout  para el cierre
         * del layout lateral lateral
         */
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Abre fragment para poder cargar llos datos
     */
    @Override
    public void abreFragment() {
        /**
         * Inicializacion de la instancia
         * {@link FragmentManager}
         * Llamado al fragment.
         */
        fragMana = getSupportFragmentManager();
        fragMana.beginTransaction().replace(R.id.contenedor, new ServiceFragment()).commit();
    }
}
