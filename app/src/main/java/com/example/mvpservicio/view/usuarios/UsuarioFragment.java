package com.example.mvpservicio.view.usuarios;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.mvpservicio.R;
import com.example.mvpservicio.UserAdpater;
import com.example.mvpservicio.Usuario;
import com.example.mvpservicio.presenter.usuarios.PresenterUserActivity;
import com.example.mvpservicio.presenter.usuarios.PresenterUserImpl;
import com.example.mvpservicio.view.registro.RegistroActivity;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Fragment creado para la visualizacion de usuarios
 */
public class UsuarioFragment extends Fragment implements UserAdapterActivity {

    /**
     * Declaracion de componentes
     * a utilizar por la clase
     */
    private UserAdpater adapter;
    final  List<Usuario> names= new ArrayList<>();
    private Realm myRealm;
    private FloatingActionButton btAdd;
    private  RecyclerView list;
    RealmResults<Usuario> result;
    PresenterUserActivity presenter;


    public UsuarioFragment() {

        // names = new ArrayList<>();
        // Required empty public constructor
    }


    /**
     * Metodo onCreate para el infado de la vista,
     * inicializacion de componentes e instancias.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_usuario, container, false);
        /**
         * Se le asigna la instancia por default
         * de REALM
         */
        myRealm = Realm.getDefaultInstance();
        list = vista.findViewById(R.id.rvlistUser);
        btAdd = vista.findViewById(R.id.btAdd);

        /**
         * Se iniciliza la intsancia de la clase
         * del presentador.
         */
        presenter = new PresenterUserImpl(this);
        myRealm.beginTransaction();
        result = myRealm.where(Usuario.class).findAll();
        names.addAll(result);
        /**
         * Inicializacion del adaptador.
         */
        adapter = new UserAdpater(names, this);
        myRealm.commitTransaction();
        list.setHasFixedSize(true);
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        list.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        /**
         * Creacion del listener
         * para el boton flotante
         */
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), RegistroActivity.class);
                startActivity(i);
            }
        });

        /**
         * Se retorna la vista
         */

        return vista;

    }


    /**
     * Implementacion del metodo para
     * la eliminacion de los usuarios.
     * recibe de parametro el usuario a borrar
     * @param usuario
     */

    @Override
    public void eliminarUsuario(String usuario) {
        presenter.eliminarUsuario(usuario);
        Log.d("mensaje2", usuario);
        Toast.makeText(getActivity(), usuario, Toast.LENGTH_SHORT).show();
    }



}

